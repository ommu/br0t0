<?php
/**
 * SettingController
 * @var $this app\components\View
 *
 * Reference start
 * TOC :
 *	Update

 *	Sublayout
 *	Pagination
 *
 * @author Putra Sudaryanto <putra@ommu.co>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.co)
 * @created date 10 May 2019, 07:57 WIB
 * @link https://bitbucket.org/ommu/br0t0
 *
 */

namespace br0t0\app\controllers;

use Yii;

class SettingController extends \app\controllers\SettingController
{
	
}

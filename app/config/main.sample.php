<?php
$params = \yii\helpers\ArrayHelper::merge(
	(\app\components\Application::isDev() && (is_readable(__DIR__ . '/../../../protected/config/params-dev.php')))?
		require(__DIR__ . '/../../../protected/config/params-dev.php'):
		require(__DIR__ . '/../../../protected/config/params.php'),
	(\app\components\Application::isDev() && (is_readable(__DIR__ . '/params-dev.php')))?
		require(__DIR__ . '/params-dev.php'):
		require(__DIR__ . '/params.php')
);
$bn = \app\components\Application::getAppId();

$config = [
	'id' => 'br0t0',
	'name' => 'OMMU',
	'runtimePath' => dirname(__DIR__) . '/runtime',
	'controllerNamespace' => 'br0t0\app\controllers',
	'bootstrap' => [],
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'b38cbed2-8332-4b84-bba0-a3b95a0858aa'
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'session' => [
			'class' => 'yii\web\Session',
			'name' => $bn,
			'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60],
			'timeout' => 7 * 24 * 3600,
			'useCookies' => true,
		],
		'jwt' => [
			'class'    => 'app\components\Jwt',
			'key'      => 'gl2J4V285U6u1GK0DCIw3tEmCYXR46AM',
			'issuer'   => 'http://br0t0.ommu.co',
			'audiance' => 'http://br0t0.ommu.co',
			'id'       => 'b38cbed2-8332-4b84-bba0-a3b95a0858aa',
		],
		'authManager' => [
			'class'             => 'mdm\admin\components\DbManager',
			'assignmentTable'   => 'ommu_core_auth_assignment',
			'itemTable'         => 'br0t0_auth_item',
			'itemChildTable'    => 'br0t0_auth_item_child',
			'ruleTable'         => 'br0t0_auth_rule',
		],
	],
	'params' => $params,
	'modules' => [
		'admin' => [
			'class' => 'app\modules\admin\Module',
		],
		'rbac' => [
			'class' => 'app\modules\rbac\Module',
			'controllerMap' => [
				'menu' => [
					'class' => 'app\modules\rbac\controllers\MenuController',
				],
			],
		],
		'user' => [
			'class' => 'app\modules\user\Module',
		],
		'report' => [
			'class' => 'ommu\report\Module',
		],
	],
];

return $config;
